// tmp1.js   -- Pruebas
// ---------------------------------
function saludar(nombre) {
    alert('Hola ' + nombre);
  }
  
  function procesarEntradaUsuario(callback) {
    var nombre = prompt('Por favor ingresa tu nombre.');
    callback(nombre);
  }
  
  procesarEntradaUsuario(saludar);

// let planti1 = {
//     T11: ["7", "NUMERO-7", "N", "7"],
//     T12: ["8", "NUMERO-8", "N", "8"],
//     T13: ["9", "NUMERO-9", "N", "9"]
// }

// let planti2 = {
//     T11: ["7", "NUMERO-7", "N", "7"],
//     T12: ["8", "NUMERO-8", "N", "8"],
//     T13: ["9", "NUMERO-9", "N", "9"]
// }

// A1 = ["7", "NUMERO-7", "N", "7"];
// A2 = ["8", "NUMERO-8", "N", "8"];

// var p1 = Object.create(planti1);
// // let p1 = A1.concat(A2);

// console.log("planti1:", planti1, p1);
// p1.T11 = ["TT", "aaaaa", "a", "aa"];
// console.log("planti2:", planti1, p1);


// function Carro(fabricante, modelo, anio) {
//     this.fabricante = fabricante;
//     this.modelo = modelo;
//     this.anio = anio;
// }

// var mycar = new Carro("Eagle", "Talon TSi", 1993);
// console.log("mycar: ", mycar, mycar.modelo);

// const res =  fetch('https://api.github.com/users/jameshibbard');
// console.log(res);

// (async () => {
//   const res = await fetch(`https://api.github.com/users/jameshibbard`);
//   const json = await res.json();
//   console.log(json.public_repos);
//   console.log("Hello!");
// })();


// function resolveAfter2Seconds(x) { 
//   return new Promise(resolve => {
//     setTimeout(() => {
//       resolve(x);
//     }, 2000);
//   });
// }

// async function f1() {
//   var x = await resolveAfter2Seconds(10);
//   console.log(x); // 10
// }
// f1();


// var Animal = '45589';
// switch (Animal) {
//   case 'Vaca':
//   case 'Jirafa':
//   case 'Perro':
//   case 'Cerdo':
//     console.log('Este animal subirá al Arca de Noé.');
//     break;
//   case 'Dinosaurio':
//   default:
//     console.log('Este animal no lo hará.');
//     console.log(Animal.split("-"));
//     let [a,b] = Animal.split("-");
//     console.log(a,b,b.length );
// }
//----------------------------------------
// var foo = 4;
// var output = 'Salida: ';
// switch (foo) {
//   case 10:
//     output += '¿Y ';
//   case 1:
//     output += 'Cuál ';
//     output += 'Es ';
//   case 2:
//     output += 'Tu ';
//   case 3:
//     output += 'Nombre';
//   case 4:
//     output += '?';
//     console.log(output);
//  //   break;
//   case 5:
//     output += '!';
//     console.log(output);
//     break;
//   default:
//     console.log('Por favor, selecciona un valor del 1 al 6.');
// }
