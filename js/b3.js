// b3.js
// ------------------------------
// carga una fila
// cantCol: cant. de columnas a crear
// filaNro: Fila a crear
// ----------------------------------
let evH2 = document.createElement('h2');
evH2.textContent = 'Teclado ESTACIONAMIENTO: 10-04-20 + https + token';
document.body.appendChild(evH2);
let evVisor1 = document.createElement('p');
evVisor1.textContent = 'DISPLAY OPERADOR';
document.body.appendChild(evVisor1);
let evVisor2 = document.createElement('p');
evVisor2.textContent = '';
document.body.appendChild(evVisor2);

let evTMP = {
  x1: "",
  x2: ""
};

// Definicion Teclado
// TECLA: [dato, extra, TIPO, SUB_TIPO]
// Donde TIPO= "N": Numero
//             "C": Comando y FIN ( ING1, EGR1)
//             "P": Comando y SIGUE (PLU, CONS)
// ---------------------------------------------
let evoTeclas = {
  T11: ["7", "NUMERO-7", "N"],
  T12: ["8", "NUMERO-8", "N"],
  T13: ["9", "NUMERO-9", "N"],
  T14: ["BORRA", "BORRA", "CLR"],
  T15: ["MOTO", "INGRESO-MOTO", "ING", "1"],
  T16: ["EGR1", "EGR-MOTO", "EGR", "0"],
  T21: ["4", "NUMERO-4", "N"],
  T22: ["5", "NUMERO-5", "N"],
  T23: ["6", "NUMERO-6", "N"],
  T24: ["-", "GUION", "N"],
  T25: ["AUTO", "INGRESO-AUTO", "ING", "2"],
  T26: ["EGR2", "EGR-AUTO", "EGR", "1"],
  T31: ["1", "NUMERO-1", "N"],
  T32: ["2", "NUMERO-2", "N"],
  T33: ["3", "NUMERO-3", "N"],
  T34: ["PAT?", "Consulta Patente", "PAT"],
  T35: ["FURG", "INGRESO-FURG", "ING", "3"],
  T36: ["EGR3", "EGR-FURG", "EGR", "2"],
  T41: ["0", "NUMERO-0", "N"],
  T42: [".", "PUNTO.DEC", "N"],
  T43: ["00", "Doble-Cero", "N"],
  T44: ["PLU", "Busca-PLU", "PLU"],
  T45: ["xx", "EGRESO-PATENTE", "x", "1"],
  T46: ["xx", "EGRESO-PATENTE", "x", "1"]
}

evoPlus = {
  "11": ["TOMATE", "45.50"],
  "22": ["LECHUGA", "75.30"]
}


// console.log(evoTeclas);

// const evURL1 = 'http://10.0.252.252'
const evURL1 = 'https://electronicavalle.com.ar'
const _getToken = '/gra/login/api/getJwtToken'       //  ?user=25&passwd=lklk00
const _ingPatente = '/tar/est/api/submitIng';       //  ?patente=466&ing_tipo=1
const _consPatente = '/tar/est/api/consPatente';     //  ?patente=466
const _consAllPatentes = '/tar/est/api/consAllPatentes' // Sin Parametros
// -------------------------------------------
// let requestURL = 'http://centrocard.evsa.com.ar/trj/public/api/visaConsulta';
// let requestURL = 'http://electronicavalle.com.ar/tar/est/api/consPatente'
// Llama a JSON
//
// ---------------------------------------------
var evTOKEN = "INICIAL";

function evfGetToken(ev_URL2, user, passwd) {
  evVisor1.textContent = `Consulta Token a: ${evURL1}`;
  let requestURL = evURL1 + ev_URL2 + `?user=${user}&passwd=${passwd}`;
  console.log("token: ", requestURL);
  let request = new XMLHttpRequest();
  request.open('GET', requestURL);
  request.responseType = 'json';

  request.onload = function () {
    let evCH2 = request.response;
    console.log(evCH2);
    evTOKEN = evCH2.token;
    evVisor1.textContent = evTOKEN;
  }
  request.send();
}

evfGetToken(_getToken, "25", "lklk00");
// evTOKEN = evVisor1.textContent;
console.log("OUT2", evTOKEN);
console.log("OUT3", evVisor1.textContent);

// -------------------------------------------------------------
function evfJson1(ev_URL2, patente, ev_DIC) {
  let requestURL = evURL1 + ev_URL2 + `&_token=${evTOKEN}`;
  //  let requestURL = evURL1+ev_URL2;
  console.log(requestURL);
  let request = new XMLHttpRequest();
  request.open('GET', requestURL, true);
  request.responseType = 'json';

  request.onload = function () {
    let evCH2 = request.response;
    console.log(evCH2);
    //  return evCH2;

    if (evCH2.error_nro == "0") {
      if (ev_DIC == "lst_mv") {
        //evVisor1.textContent = `Consulta: ${evCH2[ev_DIC][0].ing_tipo_des}-${patente}`;
        evTMP.x1 = `Ing: ${evCH2[ev_DIC][0].ing_fec}  (cant:${evCH2[ev_DIC].length})`;
        evVisor1.textContent = evTMP.x1;
        // evVisor1.autofocus;
        //evVisor2.textContent = `Ing: ${evCH2[ev_DIC][0].ing_fec}  (cant:${evCH2[ev_DIC].length})`;
        evTMP.x2 = [evCH2.error_nro, evCH2.lst_mv];           // Guardo resultado
        //location.reload();
      }
      if (ev_DIC == "dic_mv") {
        evVisor1.textContent = `Nuevo INGRESO: ${evCH2[ev_DIC].ing_tipo_des}-${patente}`;
        evVisor2.textContent = `Ing: ${evCH2[ev_DIC].ing_fec}`;
      }
    } else {
      evVisor2.textContent = evCH2.errors.patente + `: ${patente}`;
    }
  }
  request.onerror = function () {
    console.error("ERROR..: ",request.statusText);
    evVisor1.textContent='ERROR EN LA CONSULTA JSON1';
  };
  request.send();
}


function evfConsulta(cmd, patente) {
  console.log(patente.split("-"))
  let [p1, p2] = patente.split("-");
  let X1 = `?patente=${p1}`;
  if (typeof p2 !== "undefined") {
    X1 += `&patente_n=${p2}`;
  }
  evfJson1(_consPatente + X1, patente, "lst_mv");
  // evVisor1.textContent = `evTMP.x1: ${evTMP.x1}`;
  //location.reload();
}

function evfIngreso(cmd, patente, ing_tipo) {
  alert(`EJECUTA COMANDO: ${cmd}, ${patente}, ${ing_tipo}`);
  const X1 = `?patente=${patente}&ing_tipo=${ing_tipo}`;
  evfJson1(_ingPatente + X1, patente, "dic_mv");
}

function evfEgreso(cmd, patente, ing_tipo) {
  const X1 = `?patente=${patente}&ing_tipo=${ing_tipo}`;
  evfJson1(_consPatente + X1, patente, "lst_mv");
  evVisor1.textContent = evTMP.x1;
  console.log("evfEgreso.1:", evTMP.x1);
  console.log("evfEgreso.2:", evVisor2.textContent);
  const evCH = evTMP.x2;
  console.log("evCH: ", evCH);
  console.log("evCH.1: ", evCH[1]);
  console.log("evCH.1.0.tarifa0: ", evCH[1][0].lst_tarifa[0]);
  console.log("evCH.1.0.tarifa.ing_tipo: ", ing_tipo, evCH[1][0].lst_tarifa[ing_tipo]);
  const total = evCH[1][0].lst_tarifa[ing_tipo].tot;
  const CH = confirm(`EGRESA ?? : ${cmd}, ${patente}, ${ing_tipo}, ${total}`);
  if (CH) {
    evVisor1.textContent = `SALIDA: ${patente}`;
    evVisor2.textContent = `TOTAL.: ${total}`;
  } else {
    evVisor2.textContent = `CANCELA SALIDA : ${patente}`;
  }
}

// Analiza TECLA OPRIMIDA
// ----------------------------------------------
function evfAnaliza(evVisor1, evVisor2, evTecla, evB1) {
  // evVisor1.textContent = `Linea 1: ${evTecla}`;
  evVisor1.textContent = 'Ingrese Patente';
  const evCH = evoTeclas[`${evTecla}`];
  let patente = evVisor2.textContent;
  switch (evCH[2]) {
    case 'CLR':
      evVisor2.textContent = "";  // Borra linea
      break;
    case 'ING':
      // Ingreso - Cierra Operacion
      console.log("ING", evCH);
      evVisor1.textContent = evoTeclas[`${evTecla}`][1];
      evfIngreso(evCH[0], patente, evoTeclas[`${evTecla}`][3]);
      break;
    case 'EGR':
      // Egreso - Cierra Operacion
      console.log("EGR", evCH);
      evVisor1.textContent = evoTeclas[`${evTecla}`][1];
      evfEgreso(evCH[0], patente, evoTeclas[`${evTecla}`][3]);
      break;
    case 'PAT':
      // COnsulta Patente
      // evVisor1.textContent = evoTeclas[`${evTecla}`][1];
      evfConsulta(evCH[0], patente);
      break;
    case 'PLU':
      // Aqui Busco PLU y Sigue
      if (evoPlus[patente]) {
        evVisor1.textContent = `${evoPlus[patente][0]}...: ${evoPlus[patente][1]}`;
      } else {
        evVisor1.textContent = 'PLU INEXISTENTE';
      }
      break;
    case 'N':
      evVisor2.textContent += `${evB1.innerText}`;
      patente = evVisor2.textContent;
      break;
  }
}

function funFila(filaNro, cantCol, evlist1) {
  let evitem = document.createElement('li');
  let arrayFila = [];
  let i = 1;
  while (i <= cantCol) {
    let evB1 = document.createElement('button');
    let evTecla = `T${filaNro}${i}`
    evB1.textContent = evoTeclas[`${evTecla}`][0];
    evB1.onclick = function () {
      const evCH = evfAnaliza(evVisor1, evVisor2, evTecla, evB1);
      // console.log(evCH);
    }
    evitem.appendChild(evB1);
    arrayFila.push(evB1);
    i++;
  }
  evlist1.appendChild(evitem);
  //  console.log(arrayFila);
  return arrayFila
}

// filas: define la cant. de filas
// cols:  define la cant. de columnas
// --------------------------------------
function evfMatriz(filas, columnas, evHIJO1) {
  if (evHIJO1 != "XX") {
    for (const i of evHIJO1) {
      document.body.removeChild(i);
    }
  }
  let evlist1 = document.createElement('ul');
  evHIJO1 = document.body.appendChild(evlist1);
  //evh2(`--MATRIZ: ${filas} x ${columnas}--`)
  let arrayCol = [];
  let i = 1;
  while (i <= filas) {
    arrayCol.push(funFila(i, columnas, evlist1));
    i++;
  }
  let eviElem1 = document.createElement('li');
  let eviElem2 = document.createElement('h2');
  eviElem2.innerText = `--MATRIZ: ${filas} x ${columnas}--`;
  eviElem1.appendChild(eviElem2);
  evlist1.appendChild(eviElem1);

  // console.log(arrayCol);
  return [evHIJO1, arrayCol]
}

// main
// --------------------------
const MAXFILA = 4;
const MAXCOLUMAS = 6;
let evCH1 = evfMatriz(MAXFILA, MAXCOLUMAS, "XX");
//let evCH2=evfMatriz(2,3,"XX");
//let evCH3=evfMatriz(4,4,"XX");
console.log("evCH1", evCH1);
//
//evCH1=evfMatriz(2,2,[evCH3[0],evCH2[0]]);
//console.log("evCH1", evCH1);