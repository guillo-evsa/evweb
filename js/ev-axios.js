/* ev-axios.js
   Ruben Valle
   Ver. 1.0: 29-08-2019
   -------------------------
   Conjunto de funciones utiles para llamar a APIs de uso fercuente.
*/
import axios from 'axios'

/**
* Ejempos: 
* evAxio({vm: this, url: '/gra/login/api/getAppConf'})
*   .then((data) => { this.appTitle = data.appConf.appTitle })
*
* evAxio({ vm: this, url: 'gra/login/api/getAppUser', key: 'appUser' })
*   .then((data) => { this.appUser = data.appUser })
*/
// ==================================================================
export const evAxio = ({ vm, url, params, nvlError }) => {

    return new Promise((resolve, reject) => {
        axios.post(url, params)
            .then((response) => {
                // eslint-disable-next-line
                //console.log('evAxio.then')
                if (response.data.error_nro <= (nvlError || 0)) {
                    resolve(response.data)
                } else if (response.data.error_nro === 1) {
                    const error = `${response.data.error_txt}: ${JSON.stringify(response.data.errors)}`
                    vm.$root.$evModal.showError(error)
                    reject(error)
                } else {
                    vm.$root.$evModal.showError(response.data.error_txt)
                    reject(response.data.error_txt)
                }
            })
            .catch((error) => {
                // eslint-disable-next-line
                // console.log('evAxio.catch')
                vm.$root.$evModal.showAxioError(error)
                reject(error)
            })
    })
}

/**
* Ejempos:
*    evAxioFieldVldDes({
*        form: this.form,
*        field: 'pers1',
*        url: '/gra/public/api/vldDes/gra_pers/pers',
*        params: { pers: this.form.data.pers1 }
*    })
* 
*
*    loc4LovParams () {
*      return { _flt: `gra_loc.est = 'A' and gra_loc.prov = ${evDbS(this.form.data.prov, this.form.modelos.prov)}` }
*    }
*
*    evAxioFieldVldDes({
*        form: this.form,
*        field: 'loc4',
*        url: '/gra/public/api/vldDes/gra_loc/loc',
*        params: { loc: this.form.data.loc4, ...this.loc4LovParams }
*    })
*/
// ==================================================================
export const evAxioFieldVldDes = ({ form, field, url, params }) => {
    let vldData, vldError
    form.vldBegin(field)
    axios.post(url, params)
        .then((response) => {
            if (response.data.error_nro === 0) {
                vldData = response.data.dicData
            } else {
                vldError = response.data.error_txt
            }
        })
        .catch(() => { vldError = 'Error de comunicacion' })
        .finally(() => { form.vldEnd(field, vldData, vldError) })
}

