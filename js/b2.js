// b2.js
// ------------------------------

var list = document.createElement('ul');
var info = document.createElement('p');
var html = document.querySelector('html');

info.textContent = 'Below is a dynamic list. Click anywhere outside the list to add a new list item. Click an existing list item to change its text to something else.';

document.body.appendChild(info);
document.body.appendChild(list);

html.onclick = function() {
  var listItem = document.createElement('li');
  var evB1 = document.createElement('button');
//  var evB1=evBoton;
  var listContent = prompt('What content do you want the list item to have?');
//  listItem.textContent = listContent;
//  list.appendChild(listItem);
  evB1.textContent = listContent;
  listItem.appendChild(evB1)
  list.appendChild(listItem);

  listItem.onclick = function(e) {
    e.stopPropagation();
    var listContent = prompt('Enter new content for your list item');
    this.textContent = listContent;
  }
}

// carga una fila
// cantCol: cant. de columnas a crear
// filaNro: Fila a crear
// ----------------------------------
function funFila(filaNro, cantCol, evlist1) {
  var evitem = document.createElement('li');
  let arrayFila=[];
  let i=1;
  while ( i<=cantCol) {
    var evB1 = document.createElement('button');
    evB1.textContent = `bot-${filaNro}-${i}`;
//    evB1.onclick = function (i) { alert("OPRIMISTE-${i}")}
    evitem.appendChild(evB1);
    arrayFila.push(evB1);
    i++;
  }
  evlist1.appendChild(evitem);
//  console.log(arrayFila);
  return arrayFila
}

// filas: define la cant. de filas
// cols:  define la cant. de columnas
// --------------------------------------
function funMatriz(filas,columnas) {
  let evlist1 = document.createElement('ul');
  document.body.appendChild(evlist1);
  evh2(`--MATRIZ: ${filas} x ${columnas}--`)
  let arrayCol=[];
  let i=1;
  while ( i<=filas) {
    arrayCol.push(funFila(i, columnas,evlist1));
    i++;
  }
  console.log(arrayCol);
  return arrayCol
}

// main
// --------------------------
const MAXFILA=4;
const MAXCOLUMAS=5;
funMatriz(MAXFILA,MAXCOLUMAS);

let evObj1={
  n1: 10,
  n2: "pepe",
  n3: [22,33,44]
}

console.log(evObj1);
evout(evObj1.n3 + "--" + evObj1.n3[1]);
evh2(evObj1.n1**2);       // eleva a la 2
evh2(evObj1.n2);
