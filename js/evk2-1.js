// evk2-1.js
// Genera teclado dinamico
//------------------------------------------
//import evtExtra1    from './evlib1.js';
//import {evtExtra1}    from './evlib1.js';

//OK import { evtExtra1, evtEstaciona } from './evlib1.js';

import { evtEstaciona } from './evlib1.js';
import { evtExtra1 } from './evlib1.js';

console.log('evtEstaciona', evtEstaciona);



window.onload = function () { //Acciones tras cargar la página
  evfMain();
}

// ---------------------------------------------
let evoTeclas;
// evoTeclas = evtNumerico;
evoTeclas = evtEstaciona;

const evoPlus = {
  "11": ["TOMATE", "45.50"],
  "22": ["LECHUGA", "75.30"]
}

const evPublicidad = 'Electronica Valle S.A.';
let evModelo = 'Teclado EVSA evk2 V1.0';
let evVisor1;
let evVisor2;
let evDiv0;     // Contenedor Principal
let evDiv1;     // Division Visor
let evDiv2;     // Division Teclado
let evB1;       // Boton Generico
let evTecladoActivo1;    // Guarda el Ultimo Teclado 1
let evTecladoActivo2 = 'NEW';    // Guarda el Ultimo Teclado 2

let evKeyClases;   // Array de Clases Disponebles para el teclado
let evClaseActiva = 1; // Clase Activa
evKeyClases = ['evDiv2_class', 'evP1_class', 'evP2_class'];

// ------------------------
let x = "0"; //número en pantalla
let xi = 1; //iniciar número en pantalla: 1=si; 0=no;
let coma = 0; //estado coma decimal 0=no, 1=si;
let ni = 0; //número oculto o en espera.
let op = "no"; //operación en curso; "no" =  sin operación.
const evCOMA = "-";

function evfBorradoTotal() {
  //    evfVisor1(["Teclado evk2 V.1","Bienvenidos"])
  //    pantalla.innerHTML = "0"; //poner pantalla a 0
  x = "0"; //reiniciar número en pantalla
  coma = 0; //reiniciar estado coma decimal 
  ni = 0 //indicador de número oculto a 0;
  op = "no" //borrar operación en curso.
  evfVisor1([evModelo, `${Date().slice(0, 24)}`]);
  evfVisor2("0");
}


function evfVisor1(a1) {
  console.log("a1: ", a1);
  const renMAX = 3;                      // Cantidad maxima de renglones
  const lenMAX = 30;                     // Longitud Maxima del renglon
  let ren = 0;
  let i2 = "";                           // dato a mostrar
  for (const i1 of a1) {
    console.log(typeof(i1));
    // console.log('evfVisor', lenMAX, i1.length, i1.substr(0, lenMAX));
    i2 += i1.substr(0, lenMAX) + "<br>";
    ren++
  }
  while (ren < renMAX) {
    i2 += "<br>";
    ren++
  }
  evVisor1.innerHTML = i2;
}

function evfVisor2(a1) {
  evVisor2.innerHTML = a1;
}

//mostrar número en pantalla según se va escribiendo:
function evfNumero(xx) { //recoge el número pulsado en el argumento.
  if (x == "0" || xi == 1) {	// inicializar un número, 
    evVisor2.innerHTML = xx; //mostrar en pantalla
    x = xx; //guardar número
    if (xx == evCOMA) { //si escribimos una coma al principio del número
      evVisor2.innerHTML = "0" + evCOMA; //escribimos 0.
      x = xx; //guardar número
      coma = 1; //cambiar estado de la coma
    }
  }
  else { //continuar escribiendo un número
    if (xx == evCOMA && coma == 0) { //si escribimos una coma decimal pòr primera vez
      evVisor2.innerHTML += xx;
      x += xx;
      coma = 1; //cambiar el estado de la coma  
    }
    //si intentamos escribir una segunda coma decimal no realiza ninguna acción.
    else if (xx == evCOMA && coma == 1) { }
    //Resto de casos: escribir un número del 0 al 9: 	 
    else {
      evVisor2.innerHTML += xx;
      x += xx
    }
  }
  xi = 0 //el número está iniciado y podemos ampliarlo.
}

function evfConsPlu() {
  // Aqui Busco PLU y Sigue
  let evdato = evVisor2.innerHTML;
  let evPlu = evoPlus[evdato];
  // console.log("evPlu: ", evdato, evPlu);
  if (evPlu) {
    evfVisor1(['Consulta PLU', `${evPlu[0]}...: ${evPlu[1]}`]);
  } else {
    evfVisor1(['PLU INEXISTENTE']);
  }
}

function evfConsulta() {
  // Consulta Patente
  let patente = evVisor2.innerHTML;
  // console.log(patente.split("-"));
  let [p1, p2] = patente.split("-");
  let X1 = `?patente=${p1}`;
  if (typeof p2 !== "undefined") {
    X1 += `&patente_n=${p2}`;
  }
  evfVisor1(['Consulta Patente', X1]);
  // GGG Falta Completar
  //evfJson1(_consPatente + X1, patente, "lst_mv");
  // evVisor1.textContent = `evTMP.x1: ${evTMP.x1}`;
  //location.reload();
}

function evfIngreso(ing_tipo) {
  let patente = evVisor2.innerHTML;
  // console.log(patente.split("-"));
  let [p1, p2] = patente.split("-");
  const X1 = `?patente=${p1}&ing_tipo=${ing_tipo}`;
  evfVisor1([`Ingreso Patente:`, X1]);

  // Ingresa una Patente ( SIN -)
  // GGG Falta 
  // alert(`EJECUTA COMANDO: ${cmd}, ${patente}, ${ing_tipo}`);
  // const X1 = `?patente=${patente}&ing_tipo=${ing_tipo}`;
  // evfJson1(_ingPatente + X1, patente, "dic_mv");
}

function evfEgreso(ing_tipo) {
  let patente = evVisor2.innerHTML;
  let [p1, p2] = patente.split("-");
  let X1 = `?patente=${p1}`;
  if (typeof p2 !== "undefined") {
    X1 += `&patente_n=${p2}`;
  }
  evfVisor1(['Consulta Patente:', 'y ... Egreso', X1]);

  // Ingresa una Patente ( SIN -)
  // GGG Falta 
  // alert(`EJECUTA COMANDO: ${cmd}, ${patente}, ${ing_tipo}`);
  // const X1 = `?patente=${patente}&ing_tipo=${ing_tipo}`;
  // evfJson1(_ingPatente + X1, patente, "dic_mv");
}


function evfFont(ing_tipo) {
  // Agranda o Achica fonts 'ing_tipo' Veces
  const p1 = evVisor2.innerHTML;
  const n1 = Number.parseInt(p1);
  evfVisor1([`evfFont: ${p1}`]);
  if (n1 == 0) {                // Incrementa en 1
    evClaseActiva += 1;
  } else {
    evClaseActiva = n1;          // Toma el valor indicado
  }
  let l1 = evKeyClases.length;
  if (evClaseActiva >= l1) {     //  Si Supera el maximo -> tome 0
    evClaseActiva = 0;
  }
  let ch = document.getElementById('evDiv2_id');
  ch.className = evKeyClases[evClaseActiva];
  console.log("ch", evClaseActiva, evKeyClases, ch);
}

function evfAgregaTeclado(ing_tipo) {
  if (evTecladoActivo2 == 'NEW') {
    evTecladoActivo2 = evfMatriz(1, 6, evtExtra1, 'NEW');
    return evTecladoActivo2;
  }

  if (evTecladoActivo2 == 'REMOVE') {
    evTecladoActivo2 = evfMatriz(1, 6, evtExtra1, 'NEW');
  } else {
    evTecladoActivo2 = evfMatriz('REMOVE', '', '', evTecladoActivo2);  // GGG
  }
  return evTecladoActivo2;
}


// Analiza Las Funciones de cada Tecla
// -----------------------------------------------------------
function evfAnaliza(evfun, evdato, evTecla, x4) {
  x4 = evoTeclas[`${evTecla}`];
  evfVisor1([`${evfun},${evdato}`, x4.toString() ]);
  console.log(evTecla, x4);
  switch (evfun) {
    case 'CLR':
      evfBorradoTotal();
      break;
    case 'N':
      //evVisor2.innerHTML += evdato;
      evfNumero(evdato);
      break;
    case 'PAT':
      // COnsulta Patente
      evfConsulta();
      break;
    case 'PLU':
      // Consulta Plu
      evfConsPlu();
      break;
    case 'ING':
      // Ingreso - Cierra Operacion
      evfIngreso(evdato);
      break;
    case 'EGR':
      // Egreso - Cierra Operacion
      evfEgreso(evdato);
      break;
    case 'FONT':
      // Agranda o AChica Fonts Depende de evdatos "SI" o "NO"
      evfFont(evdato);
      break;
    case 'KEY':
      // Agrega Teclado Aux.
      evfAgregaTeclado(evdato);
      break;
  }
}

function evfFila2(filaNro, cantCol) {
  let evP1 = document.createElement('div');
  // evP1.className = 'evP1_class';                      // Prueba para identificar
  let i = 1;
  while (i <= cantCol) {
    const evTecla = `T${filaNro}${i}`
    const evDatos = evoTeclas[`${evTecla}`]
    // evB1 = document.createElement('button');
    evB1 = document.createElement('input');
    evB1.type = 'button';
    evB1.value = evDatos[0];
    // evB1.onclick = 'evfAnaliza(evTecla, evB1)'
    // evB1.addEventListener(onclick, evfAnaliza(evTecla, evB1));
    evB1.onclick = function () {
      const evCH = evfAnaliza(evDatos[2], evDatos[3], evTecla);
    }
    evP1.appendChild(evB1);
    i++;
  }
  evDiv2.appendChild(evP1);
}

//
function evfCrearMain(evOld) {
  if (evOld != "NEW") {
    for (const i of evOld) {
      document.body.removeChild(i);
    }
  }

  evDiv0 = document.createElement('div');
  evDiv0.className = 'evDiv0_class';
  document.body.appendChild(evDiv0);
  return evDiv0;
}


function evfCrearVisor(evText) {
  let ch = document.createElement('div');
  ch.innerHTML = evText;
  evDiv1.appendChild(ch);
  return ch;
}

function evfCrearVisores() {
  evDiv1 = document.createElement('div');
  evDiv1.className = 'evDiv1_class';
  evDiv0.appendChild(evDiv1);

  evVisor1 = evfCrearVisor('uno');
  evVisor2 = evfCrearVisor('dos');
}


// filas: define la cant. de filas
// cols:  define la cant. de columnas
// --------------------------------------
function evfMatriz(filas, cols, evoTeclas, evOld) {
  console.log("evfMatrix", filas, cols, evoTeclas, evOld);        // GGG

  if (filas == "REMOVE") {
    evDiv0.removeChild(evOld);
    return 'NEW';
  }


  if (evOld != "NEW") {
    evDiv0.removeChild(evOld);
  }

  evDiv2 = document.createElement('div');
  // evDiv2.className = 'evDiv2_class';
  evDiv2.className = evKeyClases[evClaseActiva];
  evDiv2.id = 'evDiv2_id';
  evDiv0.appendChild(evDiv2);

  let i = 1;
  while (i <= filas) {
    evfFila2(i, cols);
    i++;
  }


  let eviElem1 = document.createElement('div');
  eviElem1.innerHTML = `--MATRIZ: ${filas} x ${cols}--` + evPublicidad;
  evDiv2.appendChild(eviElem1);
  return evDiv2;
}

function evfTeclado(elEvento) {
  let evento;
  evento = elEvento || window.event;
  let k1 = evento.keyCode; //número de código de la tecla.
  let k2 = evento.key; // Simbolo
  // console.log("evento:", evento);
  evfVisor1(["key, keyCode..: ", `${k2}, ${k1}`, "123456....123456789.1234567890"]);
  //teclas númericas del teclado alfamunérico
  let p;
  if (k1 > 47 && k1 < 58) {
    p = k1 - 48; //buscar número a mostrar.
    p = String(p) //convertir a cadena para poder añádir en pantalla.
    evfNumero(p); //enviar para mostrar en pantalla
  }
  //Teclas del teclado númerico. Seguimos el mismo procedimiento que en el anterior.
  if (k1 > 95 && k1 < 106) {
    p = k1 - 96;
    p = String(p);
    evfNumero(p);
  }
  if (k1 == 110 || k1 == 190) { evfNumero(evCOMA) } //teclas (.)
  if (k1 == 109 || k1 == 189) { evfNumero(evCOMA) } //teclas (-)
  if (k1 == 219 || k1 == 107) { evfConsPlu() } // tecla (?,+) Consulta PLU
  if (k1 == 46 || k1 == 67) { evfBorradoTotal() } //Tecla borrado total: "supr"
  if (k1 == 73) { evfIngreso('1') }      // tecla (I) Ingreso tipo '1'
  if (k1 == 69) { evfEgreso('1') }       // tecla (E) Egreso  tipo '1' 
  if (k1 == 80) { evfConsulta() }        // tecla (P) Consulta Patente
  if (k1 == 38) { evfFont('') }          // tecla (FLECHA-UP)   Agranda IMAGEN 
  if (k1 == 40) { evfAgregaTeclado('') } // tecla (FLECHA-DOWN) Agrega Teclado 

}


// Funcion Arranque
function evfMain() {
  evfCrearMain('NEW');
  evfCrearVisores();
  evTecladoActivo1 = evfMatriz(4, 6, evoTeclas, 'NEW');
  // evfMatriz(1,6,evtExtra1);
  // evfMatriz(4,3,evtNumerico);
  evfBorradoTotal();
  document.onkeydown = evfTeclado;  //Habilita Captura teclado
}

