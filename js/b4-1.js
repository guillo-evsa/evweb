//  b4-1.js
// Mejoras al TECLADO
// ----------------------------------------
//------------------------------------------

window.onload = function () { //Acciones tras cargar la página
    evVISOR1 = document.getElementById("visor_id1"); //elemento pantalla de salida
    pantalla = document.getElementById("visor_id2"); //elemento pantalla de salida
    document.onkeydown = teclado;  //función teclado disponible
    this.borradoTotal();
}

// ------------------------
x = "0"; //número en pantalla
xi = 1; //iniciar número en pantalla: 1=si; 0=no;
coma = 0; //estado coma decimal 0=no, 1=si;
ni = 0; //número oculto o en espera.
op = "no"; //operación en curso; "no" =  sin operación.
const evCOMA = "-";
//let evREN1 = "Ingrese datos<br>";

// Muestra Datos 
// espera un Array

function evfVisor1 (a1){
    const renMAX=3;                      // Cantidad maxima de renglones
    let ren=0;
    let i2="";                           // dato a mostrar
    for (const i1 of a1) {
       i2 +=  i1+"<br>";
       ren++
    }
    while (ren < renMAX) {
        i2 += "<br>";
        ren++
    }
   evVISOR1.innerHTML = i2
}

//mostrar número en pantalla según se va escribiendo:
function numero(xx) { //recoge el número pulsado en el argumento.
    if (x == "0" || xi == 1) {	// inicializar un número, 
        pantalla.innerHTML = xx; //mostrar en pantalla
        x = xx; //guardar número
        if (xx == evCOMA) { //si escribimos una coma al principio del número
            pantalla.innerHTML = "0" + evCOMA; //escribimos 0.
            x = xx; //guardar número
            coma = 1; //cambiar estado de la coma
        }
    }
    else { //continuar escribiendo un número
        if (xx == evCOMA && coma == 0) { //si escribimos una coma decimal pòr primera vez
            pantalla.innerHTML += xx;
            x += xx;
            coma = 1; //cambiar el estado de la coma  
        }
        //si intentamos escribir una segunda coma decimal no realiza ninguna acción.
        else if (xx == evCOMA && coma == 1) { }
        //Resto de casos: escribir un número del 0 al 9: 	 
        else {
            pantalla.innerHTML += xx;
            x += xx
        }
    }
    xi = 0 //el número está iniciado y podemos ampliarlo.
}

function borradoTotal() {
    evfVisor1(["Teclado evk2 V.1","Bienvenidos"])
    pantalla.innerHTML = "0"; //poner pantalla a 0
    x = "0"; //reiniciar número en pantalla
    coma = 0; //reiniciar estado coma decimal 
    ni = 0 //indicador de número oculto a 0;
    op = "no" //borrar operación en curso.
}

function retro() { //Borrar sólo el último número escrito.
    cifras = x.length; //hayar número de caracteres en pantalla
    br = x.substr(cifras - 1, cifras) //describir último caracter
    x = x.substr(0, cifras - 1) //quitar el ultimo caracter
    if (x == "") { x = "0"; } //si ya no quedan caracteres, pondremos el 0
    if (br == ".") { coma = 0; } //Si el caracter quitado es la coma, se permite escribirla de nuevo.
    pantalla.innerHTML = x; //mostrar resultado en pantalla	 
}


function teclado(elEvento) {
    evento = elEvento || window.event;
    k1 = evento.keyCode; //número de código de la tecla.
    k2 = evento.key   ; // Simbolo
    console.log("evento:", evento);
    evfVisor1(["key --> keyCode",`${k2}->${k1}`,"123456....123456789.1234567890"]);
    //teclas númericas del teclado alfamunérico
    if (k1 > 47 && k1 < 58) {
        p = k1 - 48; //buscar número a mostrar.
        p = String(p) //convertir a cadena para poder añádir en pantalla.
        numero(p); //enviar para mostrar en pantalla
    }
    //Teclas del teclado númerico. Seguimos el mismo procedimiento que en el anterior.
    if (k1 > 95 && k1 < 106) {
        p = k1 - 96;
        p = String(p);
        numero(p);
    }
    // if (k1 == 110 || k == 190) { numero(".") } //teclas de coma decimal
    if (k1 == 110 || k1 == 190) { numero(evCOMA) } //teclas de coma decimal
    if (k1 == 109) { numero(evCOMA) } //teclas (-)
    if (k1 == 106) { operar('*') } //tecla multiplicación
    if (k1 == 107) { operar('+') } //tecla suma
    // if (k1 == 109) { operar('-') } //tecla resta
    if (k1 == 111) { operar('/') } //tecla división
    if (k1 == 32 || k1 == 13) { igualar() } //Tecla igual: intro o barra espaciadora
    if (k1 == 46 || k1 == 67) { borradoTotal() } //Tecla borrado total: "supr"
    if (k1 == 8) { retro() } //Retroceso en escritura : tecla retroceso.
    if (k1 == 36) { borradoParcial() } //Tecla borrado parcial: tecla de inicio.
}
