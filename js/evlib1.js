// evlib1.js
// --------------
// Definicion Teclado
// TECLA: [dato, extra, TIPO, SUB_TIPO]
// Donde TIPO= "N": Numero
//             "C": Comando y FIN ( ING1, EGR1)
//             "P": Comando y SIGUE (PLU, CONS)
// Tecla: [SIMBOLO, ACLARACION, Func, Sub.Func]
// ---------------------------------------------
let evtExtra1 = {
    T11: ["7", "NUMERO-7", "N", "7"],
    T12: ["8", "NUMERO-8", "N", "8"],
    T13: ["9", "NUMERO-9", "N", "9"],
    T14: ["BORRA", "BORRA TODO", "CLR", ""],
    T15: ["MOTO", "INGRESO-MOTO", "ING", "1"],
    T16: ["EFECT", "EGR-MOTO", "EGR", "0"],
}

let evtNumerico = {
    T11: ["7", "NUMERO-7", "N", "7"],
    T12: ["8", "NUMERO-8", "N", "8"],
    T13: ["9", "NUMERO-9", "N", "9"],
    T21: ["4", "NUMERO-4", "N", "4"],
    T22: ["5", "NUMERO-5", "N", "5"],
    T23: ["6", "NUMERO-6", "N", "6"],
    T31: ["1", "NUMERO-1", "N", "1"],
    T32: ["2", "NUMERO-2", "N", "2"],
    T33: ["3", "NUMERO-3", "N", "3"],
    T41: ["0", "NUMERO-0", "N", "0"],
    T42: ["-", "GUION", "N", "-"],
    T43: ["00", "Doble-Cero", "N", "00"]
}


let evtEstaciona = {
    T11: ["7", "NUMERO-7", "N", "7"],
    T12: ["8", "NUMERO-8", "N", "8"],
    T13: ["9", "NUMERO-9", "N", "9"],
    T14: ["BORRA", "BORRA TODO", "CLR", ""],
    T15: ["MOTO", "INGRESO-MOTO", "ING", "1"],
    T16: ["EFECT", "EGR-MOTO", "EGR", "0"],
    T21: ["4", "NUMERO-4", "N", "4"],
    T22: ["5", "NUMERO-5", "N", "5"],
    T23: ["6", "NUMERO-6", "N", "6"],
    T24: ["-", "GUION", "N", "-"],
    T25: ["AUTO", "INGRESO-AUTO", "ING", "2"],
    T26: ["CTA.CTE.", "EGR-AUTO", "EGR", "1"],
    T31: ["1", "NUMERO-1", "N", "1"],
    T32: ["2", "NUMERO-2", "N", "2"],
    T33: ["3", "NUMERO-3", "N", "3"],
    T34: ["PAT?", "Consulta Patente", "PAT", ""],
    T35: ["FURG", "INGRESO-FURG", "ING", "3"],
    T36: ["S/CARGO", "EGR-FURG", "EGR", "2"],
    T41: ["0", "NUMERO-0", "N", "0"],
    T42: ["-", "GUION", "N", "-"],
    T43: ["00", "Doble-Cero", "N", "00"],
    T44: ["PLU?", "Busca-PLU", "PLU", ""],
    T45: ["KEY", "AGREGA-Tecla123456", "KEY", "evtExtra1"],
    T46: ["++", "CAMBIA-FONT", "FONT", ""]
}

let evtBalanza = {
    T11: ["7", "NUMERO-7", "N", "7"],
    T12: ["8", "NUMERO-8", "N", "8"],
    T13: ["9", "NUMERO-9", "N", "9"],
    T14: ["BORRA", "BORRA TODO", "CLR", ""],
    T15: [" ", "", "", ""],
    T16: ["ETIQ-1", "IMPRIME-ETIQUETA", "ETIQ", "1"],
    T17: [" ", "", "", ""],
    T18: ["ETIQ-5", "IMPRIME-ETIQUETA", "ETIQ", "5"],
    T19: [" ", "", "", ""],

    T21: ["4", "NUMERO-4", "N", "4"],
    T22: ["5", "NUMERO-5", "N", "5"],
    T23: ["6", "NUMERO-6", "N", "6"],
    T24: ["CHOCO", "CHOCOLATE", "PLU1", "11"],
    T25: ["VAINI", "VAINILLA",  "PLU1", "22"],
    T26: ["LIMON", "LIMON",     "PLU1", "33"],
    T27: [" ", "", "", ""],
    T28: [" ", "", "", ""],
    T29: [" ", "", "", ""],

    T31: ["1", "NUMERO-1", "N", "1"],
    T32: ["2", "NUMERO-2", "N", "2"],
    T33: ["3", "NUMERO-3", "N", "3"],
    T34: ["MANTE", "MANTECOL", "PLU1", "44"],
    T35: ["CREMA", "CREMA AMERICANA", "PLU1", "55"],
    T36: ["ANANA", "ANANA", "PLU1", "66"],
    T37: [" ", "", "", ""],
    T38: [" ", "", "", ""],
    T39: [" ", "", "", ""],

    T41: ["0", "NUMERO-0", "N", "0"],
    T42: [" ", "", "", ""],
    T43: ["00", "Doble-Cero", "N", "00"],
    T44: ["PLU?", "Busca-PLU", "PLU", ""],
    T45: ["++", "CAMBIA-CLASE-TECLADO", "FONT", ""],
    T46: [" ", "", "", ""],
    T47: [" ", "", "", ""],
    T48: [" ", "", "", ""],
    T49: [" ", "", "", ""],
}

// export default evtEstaciona ;   // OK
// export { evtExtra1, evtEstaciona };
export { evtExtra1 };
export { evtEstaciona };
export { evtBalanza };
// import evAxio from './ev-axios.js';

// let a1=vAxio ;

let evURL1 = '';
// const evURL1 = 'http://10.0.252.252'
evURL1 = 'https://electronicavalle.com.ar';
const _getToken = '/gra/login/api/getJwtToken';     //  ?user=25&passwd=lklk00
const _ingPatente = '/tar/est/api/submitIng';       //  ?patente=466&ing_tipo=1
const _consPatente = '/tar/est/api/consPatente';     //  ?patente=466
const _consAllPatentes = '/tar/est/api/consAllPatentes' // Sin Parametros
// -------------------------------------------


// -------------------------------------------
function evfGet(url) {
    console.log('url: ', url);
    // Return a new promise.
    return new Promise(function (resolve, reject) {
        // Do the usual XHR stuff
        let req = new XMLHttpRequest();
        req.open('GET', url);
        req.onload = function () {
            // This is called even on 404 etc
            // so check the status
            if (req.status == 200) {
                // Resolve the promise with the response text
                console.log('req.response:', req.response);
                resolve(req.response);
            }
            else {
                console.log('error: ', req.statusText);
                // Otherwise reject with the status text
                // which will hopefully be a meaningful error
                reject(Error(req.statusText));
            }
        };

        // Handle network errors
        req.onerror = function () {
            reject(Error("Network Error"));
        };

        // Make the request
        req.send();
    });
}

let evOUT1;
let evOUT2;

function evfGetJson1(url1, url2, user, passwd) {
    let url = url1 + url2 + `?user=${user}&passwd=${passwd}`;
    return evfGet(url).then(JSON.parse);
}

function evfGetJson2(url1, url2, user, passwd) {
    let url = url1 + url2 + `?user=${user}&passwd=${passwd}`;
    evfGet(url).then(JSON.parse).then(function(response) {
        evOUT1 = response;
        console.log("Yey JSON!", evOUT1);
        return response;
    })
}

evOUT2 = evfGetJson1(evURL1, _getToken, '25', 'lklk00');
// evOUT2 = evOUT1.then(function(response) {
//         console.log("Yey JSON!", response);
//       })

console.log("inicio");
console.log('evOUT1: ', evOUT1);
console.log('evOUT2: ', evOUT2);
// Prueba 
// -------------------------------------
// evOUT1 = evfGetJson2(evURL1, _getToken, '25', 'lklk00').then(function (response) {
//     console.log("Success!", response);
// }, function (error) {
//     console.error("Failed!", error);
// })
// -------------------------------------


// ----------------------------------
// get('story.json').then(JSON.parse).then(function(response) {
//     console.log("Yey JSON!", response);
//   })


// evfGetJson(evURL1, _getToken, '25', 'lklk00').then(function(story) {
//     return getJSON(story.chapterUrls[0]);
//   }).then(function(chapter1) {
//     console.log("Got chapter 1!", chapter1);
//   })


// // const evURL1 = 'http://10.0.252.252'
// const evURL1 = 'https://electronicavalle.com.ar'
// const _getToken = '/gra/login/api/getJwtToken'       //  ?user=25&passwd=lklk00
// const _ingPatente = '/tar/est/api/submitIng';       //  ?patente=466&ing_tipo=1
// const _consPatente = '/tar/est/api/consPatente';     //  ?patente=466
// const _consAllPatentes = '/tar/est/api/consAllPatentes' // Sin Parametros
// // -------------------------------------------
// // let requestURL = 'http://centrocard.evsa.com.ar/trj/public/api/visaConsulta';
// // let requestURL = 'http://electronicavalle.com.ar/tar/est/api/consPatente'
// // Llama a JSON
// //
// // ---------------------------------------------
// var evTOKEN = "INICIAL";

// function evfGetToken(ev_URL2, user, passwd) {
//   evVisor1.textContent = `Consulta Token a: ${evURL1}`;
//   let requestURL = evURL1 + ev_URL2 + `?user=${user}&passwd=${passwd}`;
//   console.log("token: ", requestURL);
//   let request = new XMLHttpRequest();
//   request.open('GET', requestURL);
//   request.responseType = 'json';

//   request.onload = function () {
//     let evCH2 = request.response;
//     console.log(evCH2);
//     evTOKEN = evCH2.token;
//     evVisor1.textContent = evTOKEN;
//   }
//   request.send();
// }

// evfGetToken(_getToken, "25", "lklk00");
// // evTOKEN = evVisor1.textContent;
// console.log("OUT2", evTOKEN);
// console.log("OUT3", evVisor1.textContent);








function evout(x) {
    document.body.appendChild(document.createTextNode(x));
    document.body.appendChild(document.createElement('br'));
}

function evh1(x) {
    var ch = document.createElement('h1');
    ch.textContent = x;
    document.body.appendChild(ch);
    //    document.body.appendChild(document.createElement('br')); 
}

function evh2(x) {
    var ch = document.createElement('h2');
    ch.textContent = x;
    document.body.appendChild(ch);
    //    document.body.appendChild(document.createElement('br')); 
}

// otro forma de crear una funcion
const evalerta1 = (m1) => {
    alert(m1)
    //confirm(m1)
}

function evx2(x) {
    return x * x;
}

// export hello () => console.log("Hello World!");
// var evBoton = document.createElement('button');

// export { evout, evalerta1, evx2 }

